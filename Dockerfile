FROM apache/airflow:2.3.3
#FROM python:3.9.0-slim
WORKDIR /usr/local/airflow
COPY . /usr/local/airflow
RUN pip install -r requirements.txt