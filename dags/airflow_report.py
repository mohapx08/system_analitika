from datetime import datetime, timedelta
import pandahouse
from airflow.decorators import task, dag
import os

def get_data(query, connect):
   return pandahouse.read_clickhouse(query, connection = connect)



connect_down = os.environ.get("connect_down")

connect_up = os.environ.get("connect_up")

# Дефолтные параметры, которые прокидываются в таски
default_args = {
    'owner': 'r-bespalov-7 ',
    'depends_on_past': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'start_date': datetime(2022, 3, 10),
}

# Интервал запуска DAG
schedule_interval = '0 23 * * *'

@dag(default_args=default_args, schedule_interval=schedule_interval, catchup=False)
def dag_report_yesterday():

    @task()
    def extract_feed():
        query = """ SELECT toDate(time) as date ,
                        user_id,
                        countIf(action = 'like') AS likes,      
                        countIf(action = 'view') AS views,
                        CASE WHEN gender = 1  THEN 'male' ELSE 'female' END as gender,
                        CASE WHEN age < 18  THEN '<18' 
                            WHEN 18 <= age AND age <= 30  THEN '18...30' 
                            WHEN 30 < age AND age <= 50  THEN '31...50' 
                            ELSE '+50' END as age,
                        os
             FROM simulator_20220520.feed_actions
             WHERE date == today() - 61
             GROUP BY date, user_id, gender, age, os"""
        df_feed = get_data(query, connect_down)
        return df_feed

    @task()
    def extract_message():
        query = """Select *
                FROM (SELECT toDate(time) as date ,
                        user_id,
                        uniqExact(reciever_id) as  users_sent,
                        count(reciever_id) as messages_sent,
                        CASE WHEN gender = 1  THEN 'male' ELSE 'female' END as gender,
                        CASE WHEN age < 18  THEN '<18' 
                            WHEN 18 <= age AND age <= 30  THEN '18...30' 
                            WHEN 30 < age AND age <= 50  THEN '31...50' 
                            ELSE '+50' END as age,
                        os
                    FROM simulator_20220520.message_actions
                    WHERE date == today() - 61
                    GROUP BY date, user_id, age, gender, os) as sn
                LEFT JOIN
                (SELECT reciever_id,
                        uniqExact(user_id) as  users_received ,
                        count(user_id) as messages_received
                    FROM simulator_20220520.message_actions
                    WHERE toDate(time) == today() - 62
                    GROUP BY toDate(time), reciever_id) as rc
                ON sn.user_id = rc.reciever_id
                ORDER BY user_id """
        df_message = get_data(query, connect_down)
        return df_message

    @task
    def merge_df(df_feed, df_message):
        df_merge = df_feed.merge(df_message, 'outer', on=['user_id', 'date', 'os', 'gender', 'age']).fillna(0)
        return df_merge

    @task
    def transfrom_os(df_merge):
        df_os = df_merge[['date', 'os', 'likes', 'views', 'users_sent', 'messages_sent',\
                         'users_received', 'messages_received']] \
            .groupby(['date', 'os']) \
            .sum() \
            .reset_index()
        df_os.rename(columns={'os': 'groupby_values'}, inplace=True)
        df_os.insert(1, 'groupby', 'os')
        return df_os

    @task
    def transfrom_age(df_merge):
        df_age = df_merge[['date', 'age', 'likes', 'views', 'users_sent', 'messages_sent',\
             'users_received', 'messages_received']] \
            .groupby(['date', 'age']) \
            .sum() \
            .sort_values('age') \
            .reset_index()
        df_age.rename(columns={'age': 'groupby_values'}, inplace=True)
        df_age.insert(1, 'groupby', 'age')
        return df_age

    @task
    def transfrom_gender(df_merge):
        df_gender = df_merge[['date', 'gender', 'likes', 'views', 'users_sent', 'messages_sent',\
                             'users_received', 'messages_received']] \
            .groupby(['date', 'gender']) \
            .sum() \
            .reset_index()
        df_gender.rename(columns={'gender': 'groupby_values'}, inplace=True)
        df_gender.insert(1, 'groupby', 'gender')
        return df_gender

    @task
    def load(df_gender, df_os, df_age):
        import telegram
        df = df_gender.merge(df_os)
        msg = df.to_string()
        chat_id = 1625043687
        bot = telegram.bot(token = os.environ.get("report_bot_token"))
        bot.sendMessage(chat_id=chat_id, message='Hello')
        bot.sendMessage(chat_id=chat_id, message=msg)
    # Инициализируем таски
    df_feed = extract_feed()
    df_message = extract_message()
    df_merge = merge_df(df_feed, df_message)
    df_gender = transfrom_gender(df_merge)
    df_age = transfrom_age(df_merge)
    df_os = transfrom_os(df_merge)
    load(df_gender, df_age, df_os)


dag_report_yesterday = dag_report_yesterday()
