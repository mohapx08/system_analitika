from datetime import datetime, timedelta
import pandas as pd
import io
import seaborn as sns
from airflow.decorators import dag, task
import telebot
import matplotlib.pyplot as plt
import os


# Функция для CH
def get_data(query, connect):
    import pandahouse
    return pandahouse.read_clickhouse(query, connection = connect)



connect_down = os.environ.get("connect_down")

connect_up = os.environ.get("connect_up")

# Дефолтные параметры, которые прокидываются в таски
default_args = {
    'owner': 'r-bespalov-7 ',
    'depends_on_past': False,
    'retries': 2,
    'retry_delay': timedelta(minutes=5),
    'start_date': datetime(2022, 3, 10),
}

# Интервал запуска DAG
schedule_interval = '*/15 * * * * '

@dag(default_args=default_args, schedule_interval=schedule_interval, catchup=False)
def dag_alert():

    @task()
    def extract_all():
        query = """ SELECT toStartOfFifteenMinutes(time) AS ts, 
                    toStartOfDay(time) AS day, 
                    count(DISTINCT user_id) AS users, 
                    countIf(action = 'like') as likes, 
                    countIf(action = 'view') as views, 
                    countIf(action = 'message') as message,
                    (likes+views+message)/users as activivty
                    FROM (Select time,
                                user_id, 
                                action
                        FROM simulator_20220520.feed_actions
                        UNION ALL SELECT time , 
                                        user_id, 
                                        'message' as action
                        FROM simulator_20220520.message_actions)
                    WHERE ts >= toStartOfFifteenMinutes(date_sub(day, 66, now())) and ts < toStartOfFifteenMinutes(date_sub(day, 64, now()))
                    GROUP BY ts, day
                    ORDER BY ts"""
        df = get_data(query, connect_down)
        return df

    @task()
    def check_anomaly_quantile(df, metric, a=3, n=3):
        # функция check_anomaly_quantile предлагает алгоритм проверки значения на аномальность посредством
        # метода межквартильного размаха за период
        df['q25'] = df[metric].shift(1).rolling(n).quantile(0.25)  # скользящее значение квантиля 0.25 за период n
        df['q75'] = df[metric].shift(1).rolling(n).quantile(
            0.75)  # достаем максимальную 15-минутку из датафрейма - ту, которую будем проверять на аномальность
        df['iqr'] = df['q75'] - df['q25']
        df['up'] = df['q75'] + a * df['iqr']
        df['low'] = df['q25'] - a * df['iqr']
        df['up'] = df['up'].rolling(n, center=True).mean()
        df['low'] = df['low'].rolling(n, center=True).mean()
        if df[metric].iloc[-1] < df['low'].iloc[-2] or df[metric].iloc[-1] > df['up'].iloc[-2]:
            is_alert = 1
        else:
            is_alert = 0
        diff = 100 * max(abs(df[metric].iloc[-1] - df['low'].iloc[-2]), abs(df[metric].iloc[-1] - df['up'].iloc[-2])) / \
               df[metric].iloc[-1]
        return is_alert, df, diff

    @task()
    def check_anomaly_sigm(df, metric, a=3, n=4):
        # функция check_anomaly_sigm предлагает алгоритм проверки значения на аномальность посредством
        # правила трех сигм
        df['mean'] = df[metric].shift(0).rolling(n).mean()  # скользящее среднее за период n
        df['std'] = df[metric].shift(0).rolling(n).std()  # скользящее стандартное отклонение за период n
        df['up'] = df['mean'] + a * df['std']
        df['low'] = df['mean'] - a * df['std']
        df['up'] = df['up'].rolling(n, center=True).mean()  # устанавливаем верхние границы
        df['low'] = df['low'].rolling(n, center=True).mean()  # устанавливаем нижние границы
        if df[metric].iloc[-1] < df['low'].iloc[-2] or df[metric].iloc[-1] > df['up'].iloc[-2]:
            is_alert = 1
        else:
            is_alert = 0
        diff = 100 * max(abs(df[metric].iloc[-1] - df['low'].iloc[-3]), abs(df[metric].iloc[-1] - df['up'].iloc[-3])) / \
               df[metric].iloc[-1]
        return is_alert, df[6:], diff

    @task
    def check_anomaly(df, metric, threshold=0.3):
        # функция check_anomaly предлагает алгоритм проверки значения на аномальность посредством
        # сравнения интересующего значения со значением в это же время сутки назад
        # при желании алгоритм внутри этой функции можно изменить
        current_ts = df[
            'ts'].max()  # достаем максимальную 15-минутку из датафрейма - ту, которую будем проверять на аномальность
        day_ago_ts = current_ts - pd.DateOffset(days=1)  # достаем такую же 15-минутку сутки назад

        current_value = df[df['ts'] == current_ts][metric].iloc[
            0]  # достаем из датафрейма значение метрики в максимальную 15-минутку
        day_ago_value = df[df['ts'] == day_ago_ts][metric].iloc[
            0]  # достаем из датафрейма значение метрики в такую же 15-минутку сутки назад

        # вычисляем отклонение
        if current_value <= day_ago_value:
            diff = abs(current_value / day_ago_value - 1)
        else:
            diff = abs(day_ago_value / current_value - 1)

        # проверяем больше ли отклонение метрики заданного порога threshold
        # если отклонение больше, то вернем 1, в противном случае 0
        if diff > threshold:
            is_alert = 1
        else:
            is_alert = 0

        return is_alert, current_value, day_ago_value, diff

    def send_alert(token, chat_id, picture, message):
        token = token
        chat_id = chat_id
        bot = telebot.TeleBot(token)
        bot.send_photo(chat_id, photo=picture, caption=message)


    @task
    def run_alert(metric, is_alert_quantile, df_quantile, diff_quantile, is_alert):
        token = os.environ.get("report_bot_token")
        chat_id = 1625043687
        if is_alert_quantile == 1 and is_alert == 1:
            msg = '''Метрика {metric} за {date}:\nТекущее значение = {current_value:.2f};\
                \nПредыдущее значение = {last_value:.2f};\
                \nОтклонение по методу межквартильного размаха {quantile_value:.2f}%.'''.format(
                date=df_quantile.ts.iloc[-1].strftime("%d %b"),
                last_value=df_quantile[metric].iloc[-2],
                metric=metric,
                current_value=df_quantile[metric].iloc[-1],
                quantile_value=diff_quantile)
            sns.set(rc={'figure.figsize': (16, 10)})  # задаем размер графика
            plt.tight_layout()
            hour_time = df_quantile.ts.dt.strftime("%H:%M")
            ax = sns.lineplot(x=hour_time, y=df_quantile[metric], label=metric, linewidth=2.5)
            ax = sns.lineplot(x=hour_time, y=df_quantile.up, label='up_quantile', linestyle='dashdot', linewidth=1)
            ax = sns.lineplot(x=hour_time, y=df_quantile.low, label='low_quantile', linestyle='dashdot',
                              linewidth=1)
            for ind, label in enumerate(ax.get_xticklabels()):
                if ind % 15 == 0:
                    label.set_visible(True)
                else:
                    label.set_visible(False)
            ax.set_xlabel("Время", fontsize=12, loc='right')
            ax.set_ylabel(metric, fontsize=14, loc='top')
            ax.set_title(f'Количество {metric}', fontsize=16)
            ax.set(ylim=(0, None))
            sns.set_style("whitegrid")
            plot_object = io.BytesIO()
            ax.figure.savefig(plot_object)
            plot_object.seek(0)
            plot_object.name = f'Алерт: {metric}.png'
            plt.close()
            send_alert(token, chat_id, plot_object, msg)

    # Инициализируем таски
    df_feed = extract_all()
    is_alert, current_value, day_ago_value, diff = check_anomaly(df_feed, 'likes')
    is_alert_views, current_value_views, day_ago_value_views, diff_views = check_anomaly(df_feed, 'views')
    is_alert_quantile_like, df_quantile_like, diff_quantile_like = check_anomaly_quantile(df_feed, 'likes')
    is_alert_quantile_view, df_quantile_view, diff_quantile_view = check_anomaly_quantile(df_feed, 'views')
    run_alert('likes', is_alert_quantile_like, df_quantile_like, diff_quantile_like, is_alert)
    run_alert('views', is_alert_quantile_view, df_quantile_view, diff_quantile_view, is_alert_views)


dag_alert = dag_alert()
